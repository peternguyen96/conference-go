import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
from pprint import pprint


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{city} {state}"
    }
    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    params = {
        "q": f"{city}, {state}, {840}",
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    pprint(content)
    try:
        coords = {"lat": content[0]["lat"], "lon": content[0]['lon']}
    except (KeyError, IndexError):
        return None

    params = {
        "lat": coords["lat"],
        "lon": coords["lon"],
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {"weather": content["weather"][0]["description"]
                and content["main"]["temp"]
                }
    except (KeyError, IndexError):
        return None
